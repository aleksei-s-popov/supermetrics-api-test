<?php

declare(strict_types=1);

return [

    'preset' => 'default',
    'ide' => 'phpstorm',

    /*
    |--------------------------------------------------------------------------
    | Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may adjust all the various `Insights` that will be used by PHP
    | Insights. You can either add, remove or configure `Insights`. Keep in
    | mind, that all added `Insights` must belong to a specific `Metric`.
    |
    */

    'exclude' => [
        //  'path/to/directory-or-file'
        'phpinsights.php',
        'index.php',
        'src/App/bootstrap.php',
    ],

    'add' => [
        //  ExampleMetric::class => [
        //      ExampleInsight::class,
        //  ]
    ],

    'remove' => [
        //  ExampleInsight::class,
//            AlphabeticallySortedUsesSniff::class,
//            DeclareStrictTypesSniff::class,
//            DisallowMixedTypeHintSniff::class,
//            ForbiddenDefineFunctions::class,
//            ForbiddenNormalClasses::class,
//            ForbiddenTraits::class,
//            TypeHintDeclarationSniff::class,

    ],

    'config' => [
        //  ExampleInsight::class => [
        //      'label' => 'value',
        //  ],
    ],

];
