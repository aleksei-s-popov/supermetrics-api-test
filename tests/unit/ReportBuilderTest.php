<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Report\ReportBuilder;
use App\Report\ReportBuildingCommands\Mapper\MapByKeyValue;
use App\Report\ReportBuildingCommands\Mutator\MutateDateTimeFormatByKey;
use App\Report\ReportBuildingCommands\Mutator\MutateStrLenByKey;
use App\Report\ReportBuildingCommands\Reducer\ReduceAvg;
use App\Report\ReportBuildingCommands\Reducer\ReduceByKeyValue;
use App\Report\ReportBuildingCommands\Reducer\ReduceCount;
use App\Report\ReportBuildingCommands\Reducer\ReduceKeyOfMax;
use App\Report\ReportBuildingCommands\Reducer\ReduceMax;
use App\Report\ReportComposite;
use App\Report\ReportKeyValueDict;
use App\SupermetricsApi\Service\DummySupermetricsService;

class ReportBuilderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @throws \App\Report\Exception\UnprocessableCommand
     */
    public function testReduce(): void
    {
        $data = [1, 1, 2, 2];
        $rootReport = new ReportComposite([
            new ReportKeyValueDict($data),
            new ReportKeyValueDict($data),
        ]);

        $report = ReportBuilder::processReport($rootReport, ...[new ReduceCount()]);
        $this->assertEquals([4, 4], $report->toArray());

        $report = ReportBuilder::processReport($rootReport, ...[new ReduceAvg()]);
        $report = json_decode(json_encode($report), true);
        $this->assertEquals([1.5, 1.5], $report);

        $report = ReportBuilder::processReport($rootReport, ...[new ReduceMax()]);
        $report = json_decode(json_encode($report), true);
        $this->assertEquals([2, 2], $report);

        $report = ReportBuilder::processReport($rootReport, ...[new ReduceKeyOfMax()]);
        $report = json_decode(json_encode($report), true);
        $this->assertEquals([[2, 3], [2, 3]], $report);


        $report = ReportBuilder::processReport($rootReport, ...[new ReduceByKeyValue('2')]);
        $report = json_decode(json_encode($report), true);
        $this->assertEquals([2, 2], $report);

    }

    /**
     * @throws \App\Report\Exception\UnprocessableCommand
     */
    public function testMap(): void
    {
        $data = [1, 1, 2, 2];
        $rootReport = new ReportComposite([
            new ReportKeyValueDict($data),
            new ReportKeyValueDict($data),
        ]);

        $report = ReportBuilder::processReport($rootReport, ...[new MapByKeyValue('2')]);
        $this->assertEquals([2 => [[1, 1, 2, 2], [1, 1, 2, 2]]], $report->toArray());
    }


    /**
     * @throws \App\Report\Exception\UnprocessableCommand
     */
    public function testMutate(): void
    {
        $data1 = ['*', \DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-02')];
        $data2 = ['***', \DateTimeImmutable::createFromFormat('Y-m-d', '2019-02-03')];
        $rootReport = new ReportComposite([
            new ReportKeyValueDict($data1),
            new ReportKeyValueDict($data2),
        ]);

        $report = ReportBuilder::processReport($rootReport, ...[
            new MutateStrLenByKey('0'),
            new ReduceByKeyValue('0')
        ]);
        $this->assertEquals([1, 3], $report->toArray());

        $report = ReportBuilder::processReport($rootReport, ...[
            new MutateDateTimeFormatByKey('1', 'm'),
            new ReduceByKeyValue('1')
        ]);
        $this->assertEquals(['01', '02'], $report->toArray());

        $report = ReportBuilder::processReport($rootReport, ...[
            new MutateDateTimeFormatByKey('1', 'd'),
            new ReduceByKeyValue('1')
        ]);
        $this->assertEquals(['02', '03'], $report->toArray());
    }


    /**
     * @throws \App\Report\Exception\UnprocessableCommand
     */
    public function testCommandsSequence(): void
    {
        $dummySupermetricsService = new DummySupermetricsService();
        $posts = $dummySupermetricsService->getPosts();

        $rootPostsReport = ReportBuilder::makeRootReport(...$posts);

        $report = ReportBuilder::processReport($rootPostsReport, ...[
            new MutateDateTimeFormatByKey('createdAt', 'm'),
            new MapByKeyValue('createdAt'),
            new MutateStrLenByKey('message'),
            new ReduceByKeyValue('message'),
            new ReduceAvg(),
        ]);
        $this->assertEquals(['01' => 1.4, '02' => 5.2], $report->toArray());


        $report = ReportBuilder::processReport($rootPostsReport, ...[
            new MutateDateTimeFormatByKey('createdAt', 'm'),
            new MapByKeyValue('createdAt'),
            new MapByKeyValue('id'),
            new MutateStrLenByKey('message'),
            new ReduceByKeyValue('message'),
            new ReduceMax(),
            new ReduceKeyOfMax(),
        ]);
        $this->assertEquals(['01' => ['_3', '_4'], '02' => ['_8', '_9']], $report->toArray());

        $report = ReportBuilder::processReport($rootPostsReport, ...[
            new MutateDateTimeFormatByKey('createdAt', 'W'),
            new MapByKeyValue('createdAt'),
            new ReduceByKeyValue('id'),
            new ReduceCount(),
        ]);
        $this->assertEquals(['01' => 3, '02' => 2, '05' => 3, '06' => 2], $report->toArray());

        $report = ReportBuilder::processReport($rootPostsReport, ...[
            new MutateDateTimeFormatByKey('createdAt', 'm'),
            new MapByKeyValue('createdAt'),
            new MapByKeyValue('author'),
            new ReduceByKeyValue('id'),
            new ReduceCount(),
            new ReduceAvg(),
        ]);
        $this->assertEquals(['01' => 2.5, '02' => 2.5], $report->toArray());
    }


    protected function _before()
    {
    }

    // tests

    protected function _after()
    {
    }
}
