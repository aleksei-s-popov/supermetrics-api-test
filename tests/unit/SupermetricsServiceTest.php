<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\DTO\PostDTO;
use App\SupermetricsApi\DTO\AuthCredentialsDTO;
use App\SupermetricsApi\Service\DummySupermetricsService;
use App\SupermetricsApi\Service\SupermetricsService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class SupermetricsServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testAuth(): void
    {
        $mock = new MockHandler([
            new Response(200, [], \GuzzleHttp\json_encode(['data' => ['sl_token' => 'some token']])),
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $supermetricsService = new SupermetricsService(
            new AuthCredentialsDTO('', '', ''),
            $client
        );

        $this->assertFalse($supermetricsService->isAuthenticated());
        $supermetricsService->auth();
        $this->assertTrue($supermetricsService->isAuthenticated());
    }


    public function testGetPosts(): void
    {
        $dummySupermetricsService = new DummySupermetricsService();
        $data = $dummySupermetricsService->getRawData();

        $responses = [];

        foreach (range(0, 9) as $key) {
            $responses[] = new Response(
                200,
                [],
                \GuzzleHttp\json_encode([
                    'data' =>
                        [
                            'posts' =>
                                [
                                    [
                                        'id' => $data[$key][0],
                                        'from_id' => $data[$key][1],
                                        'message' => str_repeat('*', $data[$key][2]),
                                        'created_time' =>
                                            \DateTimeImmutable::createFromFormat('Y-m-d', $data[$key][3])
                                                ->format(DateTime::ATOM)
                                    ],
                                ],
                        ],
                ])
            );
        }

        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);

        $supermetricsService = new SupermetricsService(
            new AuthCredentialsDTO('', '', ''),
            $client
        );

        $posts = $supermetricsService->getPosts();
        /** @var PostDTO $post */
        foreach ($posts as $key => $post) {
            $this->assertEquals($data[$key][0], $post->getId());
            $this->assertEquals($data[$key][1], $post->getAuthor());
            $this->assertEquals($data[$key][2], mb_strlen($post->getMessage()));
            $this->assertEquals(
                \DateTimeImmutable::createFromFormat('Y-m-d', $data[$key][3]),
                $post->getCreatedAt()
            );
        }
    }

    protected function _before()
    {


    }

    protected function _after()
    {
    }
}
