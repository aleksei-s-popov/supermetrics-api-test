<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\DTO\PostDTO;
use App\SupermetricsApi\Service\DummySupermetricsService;

class DummySupermetricsServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testGetPosts(): void
    {
        $dummySupermetricsService = new DummySupermetricsService();
        $posts = $dummySupermetricsService->getPosts();
        $data = $dummySupermetricsService->getRawData();

        /** @var PostDTO $post */
        foreach ($posts as $key => $post) {
            $this->assertEquals($post->getId(), $data[$key][0]);
            $this->assertEquals($post->getAuthor(), $data[$key][1]);
            $this->assertEquals(mb_strlen($post->getMessage()), $data[$key][2]);
            $this->assertEquals(
                $post->getCreatedAt(),
                \DateTimeImmutable::createFromFormat('Y-m-d', $data[$key][3])
            );
        }
    }


    protected function _before()
    {


    }

    protected function _after()
    {
    }
}
