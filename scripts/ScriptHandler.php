<?php

declare(strict_types=1);

namespace Scripts;

use Composer\Script\Event;

final class ScriptHandler
{
    public static function installGitPreCommitHook(Event $event): void
    {
        if (!$event->isDevMode()) {
            return;
        }

        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        $dstPath = \dirname($vendorDir) . '/.git/hooks/pre-commit';
        $srcPath = __DIR__. '/pre-commit.dist';

        copy($srcPath, $dstPath);
        \chmod($dstPath, 0755);
    }
}
