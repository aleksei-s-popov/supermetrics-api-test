# Automaion of code style check

### For projects with legacy  

In th beginning for the year 2019, at Teacher Team, at Skyeng, we noteced
the drop of Time To Market metric for our team. We made a little investigation
and find out that we had a bottleneck in pull request discussions on code style.

So, my colleague [Rishat Girfanov](https://github.com/xbg) find an elegant way
to automate code style check using PHP code sniffer. This solution can be found
here: [teachers-conventions](https://bitbucket.org/aleksei-s-popov/teachers-conventions/)

Rishat's approach is good for introducing code style conventions to old and even
legacy projects because it forces tho styles only for files thar have been changed
in the exact commit.   

### For new or small or well maintained projects  

There is a tool [PHP Insights](https://phpinsights.com/) that I used to check
my test submission code style (this repo). It is well documented, simple yet 
powerful and well customisable.
To automate code quality and style check, I added this command to pre-commit 
hook:
```bash
vendor/bin/phpinsights
````  

I used phpinsights default rules whithout any customisation so I had to set
with style quality threshold to `--min-style=85`. But with some configuration
threshold could be set to 100 and no style violations will be allowed.

In my opinion, this approach is best for new or well maintained projects
because it checks the whole project codebase. But if threshold is set low 
enough in the begging and increments from time to time
(during refactoring commits) it might be used for any project. 

### Configuration

Both of the above might work right out of the box, but I strongly recommend
to spend some time on style rules configurations to make the best fit for exact
project.


