<?php

declare(strict_types=1);

require 'src/app/bootstrap.php';


//$handler = new \App\Handler\StatsHandler(
//    new \App\SupermetricsApi\Service\DummySupermetricsService()
//);


$handler = new \App\Handler\StatsHandler(
    new \App\SupermetricsApi\Service\SupermetricsService(
        new \App\SupermetricsApi\DTO\AuthCredentialsDTO(
            'ju16a6m81mhid5ue1z3v2g0uh',
            'your@email.address',
            'Your Name'
        ),
        new \GuzzleHttp\Client([
            'timeout'  => 2.0,
        ])
)
);

echo json_encode($handler->getLastPostStats());
