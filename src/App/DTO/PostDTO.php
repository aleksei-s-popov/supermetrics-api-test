<?php

declare(strict_types=1);

namespace App\DTO;

use App\Report\IArrayable;

final class PostDTO implements IArrayable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @param string $id
     * @param string $author
     * @param string $message
     * @param \DateTimeImmutable $createdAt
     */
    public function __construct(string $id, string $author, string $message, \DateTimeImmutable $createdAt)
    {
        $this->id = $id;
        $this->author = $author;
        $this->message = $message;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function toArray(): array {
        return [
            'id' => $this->id,
            'author' => $this->author,
            'message' => $this->message,
            'createdAt' => $this->createdAt,
        ];
    }

}
