<?php

declare(strict_types=1);

namespace App\SupermetricsApi\Exception;

final class SupermetricsApiError extends \Exception
{
    public const ERROR_GENERAL = 'Unable to get posts';
    public const ERROR_NA = 'Unknown service error';
    public const ERROR_DATA_STRUCTURE =
        'Invalid service response data structure';

    public const CODE_NOT_200 = 10;
    public const CODE_DATA_STRUCTURE = 20;
    public const CODE_GUZZLE_EXCEPTION = 30;
}
