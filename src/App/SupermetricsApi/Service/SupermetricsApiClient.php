<?php

declare(strict_types=1);

namespace App\SupermetricsApi\Service;

use App\DTO\PostDTO;
use App\SupermetricsApi\DTO\AuthCredentialsDTO;
use App\SupermetricsApi\Exception\SupermetricsApiError;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Psr\Http\Message\ResponseInterface;

final class SupermetricsApiClient
{
    private const TOKEN_URI = 'https://api.supermetrics.com/assignment/register';
    private const POSTS_URI = 'https://api.supermetrics.com/assignment/posts';

    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(
        Client $client
    )
    {
        $this->client = $client;
    }

    /**
     * @param array<int> $pages
     * @param string $slToken
     *
     * @return array<PostDTO>
     *
     * @throws SupermetricsApiError
     * @throws \Throwable
     */
    public function getPosts(array $pages, string $slToken): array
    {
        $promises = [];

        foreach ($pages as $page) {
            $promises[] = $this->client->getAsync(
                self::POSTS_URI,
                ['query' => ['page' => $page, 'sl_token' => $slToken]]
            );
        }

        $results = Promise\unwrap($promises);
        $posts = [];
        foreach ($results as $response) {
            $posts[] = $this->getPostsFromResponse($response);
        }
        return array_merge(...$posts);
    }

    /**
     * @param $response
     *
     * @return array<mixed>
     *
     * @throws SupermetricsApiError
     */
    private function getPostsFromResponse($response): array
    {
        $data = $this->getResponseData($response);

        if (!array_key_exists('data', $data)
            || !array_key_exists('posts', $data['data'])) {
            throw new SupermetricsApiError(
                SupermetricsApiError::ERROR_DATA_STRUCTURE
                . ' :: '
                . json_encode($data),
                SupermetricsApiError::CODE_DATA_STRUCTURE
            );
        }
        return $data['data']['posts'];
    }

    /**
     * @param ResponseInterface $response
     *
     * @return array<array>
     *
     * @throws SupermetricsApiError
     */
    private function getResponseData(ResponseInterface $response): array
    {
        $data = \GuzzleHttp\json_decode($response->getBody(), true);

        if ($response->getStatusCode() !== 200) {
            $this->throwBadResponseError($data);
        }
        return $data;
    }

    /**
     * @param $data
     *
     * @throws SupermetricsApiError
     */
    private function throwBadResponseError($data): void
    {
        if (array_key_exists('error', $data)) {
            throw new SupermetricsApiError(
                $data['error'],
                SupermetricsApiError::CODE_NOT_200
            );
        }

        throw new SupermetricsApiError(
            SupermetricsApiError::ERROR_NA,
            SupermetricsApiError::CODE_NOT_200
        );
    }


    /**
     * @param AuthCredentialsDTO $authCredential
     *
     * @return string
     *
     * @throws SupermetricsApiError
     */
    public function auth(AuthCredentialsDTO $authCredential): string
    {
        $data = $this->fetchToken($authCredential);

        if (!array_key_exists('data', $data)
            || !array_key_exists('sl_token', $data['data'])) {
            throw new SupermetricsApiError(
                SupermetricsApiError::ERROR_DATA_STRUCTURE
                . ' :: '
                . json_encode($data),
                SupermetricsApiError::CODE_DATA_STRUCTURE
            );
        }

        return $data['data']['sl_token'];
    }

    /**
     * @param AuthCredentialsDTO $authCredentials
     * @return array<array>
     *
     * @throws SupermetricsApiError
     */
    private function fetchToken(AuthCredentialsDTO $authCredentials): array
    {
        $response = $this->client->post(
            self::TOKEN_URI,
            [
                'form_params' => [
                    'client_id' => $authCredentials->getClientId(),
                    'email' => $authCredentials->getEmail(),
                    'name' => $authCredentials->getName(),
                ],
            ]
        );
        return $this->getResponseData($response);
    }
}
