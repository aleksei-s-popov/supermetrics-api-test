<?php

declare(strict_types=1);

namespace App\SupermetricsApi\Service;

use App\DTO\PostDTO;
use App\SupermetricsApi\DTO\AuthCredentialsDTO;
use App\SupermetricsApi\Exception\SupermetricsApiError;
use GuzzleHttp\Client;
use Nette\Utils\DateTime;

final class SupermetricsService implements ISupermetricsService
{
    private const NO_TOKEN = '';

    /**
     * @var AuthCredentialsDTO
     */
    private $authCredentials;

    /**
     * @var string
     */
    private $slToken = self::NO_TOKEN;

    /**
     * var SupermetricsApiClient
     */
    private $supermetricsApiClient;

    /**
     * @param AuthCredentialsDTO $authCredentials
     *
     * @param Client $client
     */
    public function __construct(
        AuthCredentialsDTO $authCredentials,
        Client $client
    )
    {
        $this->authCredentials = $authCredentials;
        $this->supermetricsApiClient = new SupermetricsApiClient($client);
    }

    /**
     * @return array<PostDTO>
     *
     * @throws SupermetricsApiError
     * @throws \Throwable
     */
    public function getPosts(): array
    {
        $data = $this->supermetricsApiClient->getPosts(range(1, 10), $this->slToken);
        return $this->getPostsFromResponseData($data);
    }

    /**
     * @param array<array<string>> $data
     *
     * @return array
     */
    private function getPostsFromResponseData(array $data): array
    {
        $posts = [];
        foreach ($data as $item) {
            $posts[] = new PostDTO(
                $item['id'],
                $item['from_id'],
                $item['message'],
                \DateTimeImmutable::createFromFormat(
                    DateTime::ATOM,
                    $item['created_time']
                )
            );
        }
        return $posts;
    }

    /**
     * @return void
     *
     * @throws SupermetricsApiError
     */
    public function auth(): void
    {
        $this->slToken = $this->supermetricsApiClient->auth($this->authCredentials);
    }

    public function isAuthenticated(): bool
    {
        return $this->slToken !== self::NO_TOKEN;
    }
}
