<?php

declare(strict_types=1);

namespace App\SupermetricsApi\Service;

use App\DTO\PostDTO;
use App\SupermetricsApi\Exception\SupermetricsApiError;

interface ISupermetricsService
{
    /**
     * @return void
     *
     * @throws SupermetricsApiError
     */
    public function auth(): void;

    /**
     * @return array<PostDTO>
     *
     * @throws SupermetricsApiError
     */
    public function getPosts(): array;
}
