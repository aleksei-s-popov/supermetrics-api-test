<?php

declare(strict_types=1);

namespace App\SupermetricsApi\Service;

use App\DTO\PostDTO;

final class DummySupermetricsService implements ISupermetricsService
{
    /**
     * @return array<PostDTO>
     */
    public function getPosts(): array
    {
        $posts = $this->getRawData();
        foreach ($posts as $key => $post) {
            $posts[$key] = new PostDTO(
                $post[0],
                $post[1],
                str_repeat('*', $post[2]),
                \DateTimeImmutable::createFromFormat('Y-m-d', $post[3])
            );
        }

        return $posts;
    }

    /**
     * @return array<string|int>
     */
    public function getRawData(): array
    {
        return [
            ['_0', 'a', 1, '2019-01-01'],
            ['_1', 'a', 1, '2019-01-01'],
            ['_2', 'a', 1, '2019-01-01'],
            ['_3', 'b', 2, '2019-01-10'],
            ['_4', 'b', 2, '2019-01-10'],
            ['_5', 'a', 2, '2019-02-01'],
            ['_6', 'a', 4, '2019-02-01'],
            ['_7', 'a', 4, '2019-02-01'],
            ['_8', 'b', 8, '2019-02-10'],
            ['_9', 'b', 8, '2019-02-10'],
        ];
    }

    /**
     * @return void
     */
    public function auth(): void
    {
    }
}
