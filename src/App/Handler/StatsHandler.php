<?php

declare(strict_types=1);

namespace App\Handler;

use App\Report\Exception\UnprocessableCommand;
use App\Report\Exception\UnprocessableOperation;
use App\Report\ReportBuilder;
use App\Report\ReportBuildingCommands\Mapper\MapByKeyValue;
use App\Report\ReportBuildingCommands\Mapper\MapPostById;
use App\Report\ReportBuildingCommands\Mapper\MapPostByMonth;
use App\Report\ReportBuildingCommands\Mapper\MapPostByUser;
use App\Report\ReportBuildingCommands\Mapper\MapPostByWeek;
use App\Report\ReportBuildingCommands\Mutator\MutateDateTimeFormatByKey;
use App\Report\ReportBuildingCommands\Mutator\MutateStrLenByKey;
use App\Report\ReportBuildingCommands\Reducer\ReduceAvg;
use App\Report\ReportBuildingCommands\Reducer\ReduceByKeyValue;
use App\Report\ReportBuildingCommands\Reducer\ReduceCount;
use App\Report\ReportBuildingCommands\Reducer\ReduceKeyOfMax;
use App\Report\ReportBuildingCommands\Reducer\ReduceMax;
use App\SupermetricsApi\Exception\SupermetricsApiError;
use App\SupermetricsApi\Service\ISupermetricsService;

final class StatsHandler
{
    /**
     * @var ISupermetricsService
     */
    private $supermetricsService;

    /**
     * @param ISupermetricsService $supermetricsService
     */
    public function __construct(ISupermetricsService $supermetricsService)
    {
        $this->supermetricsService = $supermetricsService;
    }

    /**
     * @return array
     *
     * @throws SupermetricsApiError
     * @throws UnprocessableOperation
     */
    public function getLastPostStats(): array
    {

        $this->supermetricsService->auth();
        $posts = $this->supermetricsService->getPosts();

        $rootPostsReport = ReportBuilder::makeRootReport(...$posts);

        $report = [];

        try {
            $report['Average character length of a post / month'] = ReportBuilder::processReport($rootPostsReport, ...[
                new MutateDateTimeFormatByKey('createdAt', 'm'),
                new MapByKeyValue('createdAt'),
                new MutateStrLenByKey('message'),
                new ReduceByKeyValue('message'),
                new ReduceAvg(),
            ]);
        } catch (UnprocessableCommand $exception) {
            $report['Average character length of a post / month'] = $exception->getMessage();
        }

        try {
            $report['Longest post by character length / month'] = ReportBuilder::processReport($rootPostsReport, ...[
                new MutateDateTimeFormatByKey('createdAt', 'm'),
                new MapByKeyValue('createdAt'),
                new MapByKeyValue('id'),
                new MutateStrLenByKey('message'),
                new ReduceByKeyValue('message'),
                new ReduceMax(),
                new ReduceKeyOfMax(),
            ]);
        } catch (UnprocessableCommand $exception) {
            $report['Average character length of a post/month'] = $exception->getMessage();
        }

        try {
            $report['Total posts split by week'] = ReportBuilder::processReport($rootPostsReport, ...[
                new MutateDateTimeFormatByKey('createdAt', 'W'),
                new MapByKeyValue('createdAt'),
                new ReduceByKeyValue('id'),
                new ReduceCount(),
            ]);
        } catch (UnprocessableCommand $exception) {
            $report['Total posts split by week'] = $exception->getMessage();
        }

        try {
            $report['Average number of posts per user/month'] = ReportBuilder::processReport($rootPostsReport, ...[
                new MutateDateTimeFormatByKey('createdAt', 'm'),
                new MapByKeyValue('createdAt'),
                new MapByKeyValue('author'),
                new ReduceByKeyValue('id'),
                new ReduceCount(),
                new ReduceAvg(),
            ]);
        } catch (UnprocessableCommand $exception) {
            $report['Average number of posts per user/month'] = $exception->getMessage();
        }

        return $report;
    }
}
