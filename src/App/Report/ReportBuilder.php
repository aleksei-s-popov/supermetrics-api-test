<?php

declare(strict_types=1);

namespace App\Report;

use App\Report\Exception\UnprocessableCommand;
use App\Report\ReportBuildingCommands\IReportBuilderCommand;

final class ReportBuilder
{

    /**
     * @param IArrayable ...$objects
     *
     * @return ReportComposite
     */
    public static function makeRootReport(IArrayable ...$objects): ReportComposite
    {
        $reportValues = [];

        foreach ($objects as $object) {
            $reportValues[] = new ReportKeyValueDict($object->toArray());
        }

        return new ReportComposite($reportValues);
    }

    /**
     * @param IReport $report
     * @param array<IReportBuilderCommand> $commands
     *
     * @return IReport
     *
     * @throws Exception\UnprocessableOperation
     * @throws UnprocessableCommand
     */
    public static function processReport(IReport $report, IReportBuilderCommand ...$commands): IReport
    {
        foreach ($commands as $command) {
            $report = self::applyCommand($report, $command);
        }

        return $report;
    }

    /**
     * @param IReport $report
     * @param IReportBuilderCommand $command
     *
     * @return IReport
     *
     * @throws Exception\UnprocessableOperation
     * @throws UnprocessableCommand
     */
    private static function applyCommand(IReport $report, IReportBuilderCommand $command): IReport
    {
        if ($report instanceof ReportComposite
            && $command->isCompositeLevelCommand()
            && $report->isLastLevelComposite()
        ) {
            return $command->execute($report);
        }

        if ($report instanceof ReportKeyValueDict) {
            return $command->execute($report);
        }

        $subReports = $report->getValue();

        foreach ($subReports as $key => $subReport) {
            $subReports[$key] = self::applyCommand($subReport, $command);
        }

        return new ReportComposite($subReports);
    }
}
