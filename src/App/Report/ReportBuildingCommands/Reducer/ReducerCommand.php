<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Reducer;

use App\Report\Exception\UnprocessableCommand;
use App\Report\IReport;
use App\Report\ReportBuildingCommands\BaseReportBuilderCommand;

abstract class ReducerCommand extends BaseReportBuilderCommand
{
    /**
     * @param IReport $report
     *
     * @return IReport
     *
     * @throws UnprocessableCommand
     */
    public function execute(IReport $report): IReport
    {
        $this->checkReport($report);
        return $this->getValue($report);
    }

    abstract public function getValue(IReport $report): IReport;
}
