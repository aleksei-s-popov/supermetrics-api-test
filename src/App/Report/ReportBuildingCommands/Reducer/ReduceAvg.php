<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Reducer;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class ReduceAvg extends ReducerCommand
{
    /**
     * @param IReport $report
     *
     * @return bool
     */
    public function canProcess(IReport $report): bool
    {
        /** @var array<ReportKeyValueDict> $keyValues */
        $keyValues = $report->getValue();

        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $keyValue) {
            foreach ($keyValue->getValue() as $value) {
                if (!\is_numeric($value)) {
                    return false;
                }
            }

        }
        return \count($keyValues) > 0;
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    public function getValue(IReport $report): IReport
    {
        /** @var array<ReportKeyValueDict> $keyValues */
        $keyValues = $report->getValue();

        $averages = [];

        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $key => $keyValue) {
            $values = $keyValue->getValue();
            $averages[$key] = array_sum($values) / \count($values);
        }

        return new ReportKeyValueDict($averages);
    }
}
