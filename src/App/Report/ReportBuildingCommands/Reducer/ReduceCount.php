<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Reducer;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class ReduceCount extends ReducerCommand
{
    /**
     * @param IReport $report
     *
     * @return bool
     */
    public function canProcess(IReport $report): bool
    {
        return \is_array($report->getValue());
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    public function getValue(IReport $report): IReport
    {
        $keyValues = $report->getValue();
        $values = [];
        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $key => $keyValue) {
            $values[$key] = \count($keyValue->getValue());
        }

        return new ReportKeyValueDict($values);
    }
}
