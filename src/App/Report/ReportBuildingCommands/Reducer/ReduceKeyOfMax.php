<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Reducer;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class ReduceKeyOfMax extends ReducerCommand
{
    /**
     * @param IReport $report
     *
     * @return bool
     */
    public function canProcess(IReport $report): bool
    {
        /** @var array<ReportKeyValueDict> $keyValues */
        $keyValues = $report->getValue();

        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $keyValue) {
            foreach ($keyValue->getValue() as $value) {
                if (!\is_numeric($value)) {
                    return false;
                }
            }

        }
        return \count($keyValues) > 0;
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    public function getValue(IReport $report): IReport
    {
        $keyValues = $report->getValue();
        $keysOfMax = [];
        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $key => $keyValue) {
            $values = $keyValue->getValue();
            $keysOfMax[$key] = array_keys($values, max($values));
        }

        return new ReportKeyValueDict($keysOfMax);
    }
}
