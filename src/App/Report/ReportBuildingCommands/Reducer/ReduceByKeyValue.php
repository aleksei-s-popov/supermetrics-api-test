<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Reducer;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class ReduceByKeyValue extends ReducerCommand
{
    /**
     * @var string
     */
    private $key;

    /**
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param IReport $item
     *
     * @return bool
     */
    public function canProcess(IReport $item): bool
    {
        /** @var array<ReportKeyValueDict> $keyValues */
        $keyValues = $item->getValue();

        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $keyValue) {
            if (array_key_exists($this->key, $keyValue->getValue())) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    public function getValue(IReport $report): IReport
    {
        $keyValues = $report->getValue();
        $values = [];
        /** @var ReportKeyValueDict $keyValue */
        foreach ($keyValues as $key => $keyValue) {
            $values[$key] = $keyValue->getValue()[$this->key];
        }

        return new ReportKeyValueDict($values);
    }
}
