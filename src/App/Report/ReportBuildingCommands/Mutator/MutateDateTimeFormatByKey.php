<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Mutator;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class MutateDateTimeFormatByKey extends MutatorCommand
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $format;

    /**
     * @param string $key
     * @param string $format
     */
    public function __construct(string $key, string $format)
    {
        $this->key = $key;
        $this->format = $format;
    }

    /**
     * @param IReport $item
     *
     * @return bool
     */
    public function canProcess(IReport $item): bool
    {
        $itemValues = $item->getValue();
        return array_key_exists($this->key, $itemValues)
            && $itemValues[$this->key] instanceof \DateTimeInterface;
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    public function getValue(IReport $report): IReport
    {
        $itemValues = $report->getValue();
        /** @var \DateTimeInterface $date */
        $date = $itemValues[$this->key];
        $itemValues[$this->key] = $date->format($this->format);

        return new ReportKeyValueDict($itemValues);
    }
}
