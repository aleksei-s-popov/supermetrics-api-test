<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Mutator;

use App\Report\Exception\UnprocessableCommand;
use App\Report\IReport;
use App\Report\ReportBuildingCommands\BaseReportBuilderCommand;

abstract class MutatorCommand extends BaseReportBuilderCommand
{
    /**
     * @param IReport $report
     *
     * @return IReport
     *
     * @throws UnprocessableCommand
     */
    public function execute(IReport $report): IReport
    {
        $this->checkReport($report);
        return $this->getValue($report);
    }

    /**
     * @return bool
     */
    public function isCompositeLevelCommand(): bool
    {
        return false;
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    abstract public function getValue(IReport $report): IReport;
}
