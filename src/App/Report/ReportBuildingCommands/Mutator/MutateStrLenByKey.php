<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Mutator;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class MutateStrLenByKey extends MutatorCommand
{
    /**
     * @var string
     */
    private $key;

    /**
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param IReport $item
     *
     * @return bool
     */
    public function canProcess(IReport $item): bool
    {
        $itemValues = $item->getValue();
        return array_key_exists($this->key, $itemValues)
            && \is_string($itemValues[$this->key]);
    }

    /**
     * @param IReport $report
     *
     * @return IReport
     */
    public function getValue(IReport $report): IReport
    {
        $itemValues = $report->getValue();
        $itemValues[$this->key] = mb_strlen($itemValues[$this->key]);

        return new ReportKeyValueDict($itemValues);
    }
}
