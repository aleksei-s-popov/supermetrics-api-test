<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Mapper;

use App\Report\Exception\UnprocessableCommand;
use App\Report\IReport;
use App\Report\ReportBuildingCommands\BaseReportBuilderCommand;
use App\Report\ReportComposite;
use App\Report\ReportKeyValueDict;

abstract class MapperCommand extends BaseReportBuilderCommand
{
    /**
     * @param IReport $report
     *
     * @return IReport
     *
     * @throws UnprocessableCommand
     */
    public function execute(IReport $report): IReport
    {
        $this->checkReport($report);

        /** @var array<ReportKeyValueDict> $subReports */
        $subReports = $report->getValue();
        $mappedReports = [];
        /** @var ReportKeyValueDict $subReport */
        foreach ($subReports as $subReport) {

            $key = $this->getKey($subReport);

            if (array_key_exists($key, $mappedReports)) {
                $mappedReports[$key][] = $subReport;
            } else {
                $mappedReports[$key] = [$subReport];
            }
        }

        foreach ($mappedReports as $key => $mappedReportItems) {
            $mappedReports[$key] = new ReportComposite($mappedReportItems);
        }

        return new ReportComposite($mappedReports);
    }

    /**
     * @param IReport $item
     *
     * @return string
     */
    abstract protected function getKey(IReport $item): string;
}
