<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands\Mapper;

use App\Report\IReport;
use App\Report\ReportKeyValueDict;

final class MapByKeyValue extends MapperCommand
{
    /**
     * @var string
     */
    private $key;

    /**
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param IReport $item
     *
     * @return bool
     */
    public function canProcess(IReport $item): bool
    {
        $keyValues = $item->getValue();
        /** @var ReportKeyValueDict $keyValue */
        $keyOccurrences = 0;
        foreach ($keyValues as $keyValue) {
            if (array_key_exists($this->key, $keyValue->getValue())) {
                $keyOccurrences++;
            }
        }

        return $keyOccurrences === \count($keyValues);
    }

    /**
     * @param IReport $item
     *
     * @return string
     */
    public function getKey(IReport $item): string
    {
        return (string)$item->getValue()[$this->key];
    }
}
