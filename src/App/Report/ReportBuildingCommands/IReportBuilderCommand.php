<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands;

use App\Report\Exception\UnprocessableCommand;
use App\Report\IReport;

interface IReportBuilderCommand
{
    /**
     * @param IReport $report
     *
     * @return IReport
     *
     * @throws UnprocessableCommand
     */
    public function execute(IReport $report): IReport;

    /**
     * @return bool
     */
    public function isCompositeLevelCommand(): bool;
}
