<?php

declare(strict_types=1);

namespace App\Report\ReportBuildingCommands;

use App\Report\Exception\UnprocessableCommand;
use App\Report\IReport;
use App\Report\ReportComposite;
use App\Report\ReportKeyValueDict;

abstract class BaseReportBuilderCommand implements IReportBuilderCommand
{
    /**
     * @param IReport $report
     *
     * @return IReport
     */
    abstract public function execute(IReport $report): IReport;

    /**
     * @param IReport $report
     *
     * @throws UnprocessableCommand
     */
    protected function checkReport(IReport $report): void
    {
        if (!($this->isCompositeLevelCommand()
                ? $report instanceof ReportComposite
                : $report instanceof ReportKeyValueDict)
            || !$this->canProcess($report)
        ) {
            throw new UnprocessableCommand(static::class, $report);
        }
    }

    /**
     * @return bool
     */
    public function isCompositeLevelCommand(): bool
    {
        return true;
    }

    /**
     * @param IReport $item
     *
     * @return bool
     */
    abstract protected function canProcess(IReport $item): bool;
}
