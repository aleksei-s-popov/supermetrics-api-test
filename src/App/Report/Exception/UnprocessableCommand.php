<?php

declare(strict_types=1);

namespace App\Report\Exception;

final class UnprocessableCommand extends \Exception
{
    /**
     * @param string $commandClass
     * @param $data
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(
        string $commandClass,
        $data,
        $code = 0,
        ?\Throwable $previous = null
    ) {
        parent::__construct(
            'Unprocessable Command Exception :: '
                . $commandClass . ' :: '
                . json_encode($data),
            $code,
            $previous
        );
    }

}
