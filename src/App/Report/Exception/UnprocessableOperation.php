<?php

declare(strict_types=1);

namespace App\Report\Exception;

final class UnprocessableOperation extends \Exception
{
}
