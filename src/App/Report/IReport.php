<?php

declare(strict_types=1);

namespace App\Report;

interface IReport extends \JsonSerializable, IArrayable
{
    /**
     * @return array<mixed>
     */
    public function getValue(): array;

    public function isLastLevelComposite(): bool;
}
