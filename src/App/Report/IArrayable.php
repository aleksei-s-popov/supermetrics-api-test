<?php

declare(strict_types=1);

namespace App\Report;

interface IArrayable
{
    /**
     * @return array
     */
    public function toArray(): array;
}
