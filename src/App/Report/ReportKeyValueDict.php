<?php

declare(strict_types=1);

namespace App\Report;

use App\Report\Exception\UnprocessableOperation;

final class ReportKeyValueDict implements IReport
{
    /**
     * @var array<int|float|string|\DateTimeImmutable> $values
     */
    private $values;

    /**
     * @param array<int|float|string|\DateTimeImmutable> $values
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }

    public function jsonSerialize()
    {
        return $this->values;
    }

    /**
     * @return array<int|float|string|\DateTimeImmutable> $values
     */
    public function getValue(): array
    {
        return $this->values;
    }

    /**
     * @throws UnprocessableOperation
     */
    public function isLastLevelComposite(): bool
    {
        throw new UnprocessableOperation('ReportKeyValueDict is not Composite');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->values;
    }
}
