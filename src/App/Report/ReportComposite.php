<?php

declare(strict_types=1);

namespace App\Report;

final class ReportComposite implements IReport
{
    /**
     * @var array<IReport>
     */
    private $values;

    /**
     * @param IReport ...$values
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }

    public function jsonSerialize()
    {
        return $this->values;
    }

    /**
     * @return array<IReport>
     */
    public function getValue(): array
    {
        return $this->values;
    }

    public function isLastLevelComposite(): bool
    {
        foreach ($this->values as $value) {
            if (! ($value instanceof ReportKeyValueDict)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = [];
        foreach ($this->values as $key => $value) {
            $array[$key] = $value->toArray();
        }
        return $array;
    }
}
