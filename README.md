# Coding assignment from Supermetrics

Fetch and manipulate JSON data from a fictional Supermetrics Social Network REST API.

## Getting Started

1. Git clone the repo and start
1. Run `composer install` (it will also install git pre-commit hook)
1. Start a web server from root directory. E.g: `php -S localhost:1234`
1. Ger data fetching and manipulation results by GET request to root address. E.g `curl localhost:1234
`

### Prerequisites

You will need [composer](https://getcomposer.org/) & [php 7.3](https://www.php.net/)

## Running the tests

To run unit tests call codeception
```
php vendor/bin/codecept run
```


### And coding style tests

To run code quality tests call phpinsights (basic config is used)
```
vendor/bin/phpinsights
```

## Built With

* [guzzlehttp/guzzle](https://packagist.org/packages/guzzlehttp/guzzle) - PHP HTTP client 


## Authors

* **Aleksei S. Popov** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
